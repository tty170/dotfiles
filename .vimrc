" vi互換にしない
set nocompatible

" Windwos系OSのフラグ
let s:is_win = has('win32') || has('win64')
" *nix系OSフラグ
let s:is_unix = has('unix')
" MacOS Xフラグ
let s:is_mac = has('macunix')

"----------------------------------------
" ユーザーランタイムパス設定
"----------------------------------------
"Windows, unixでのruntimepathの違いを吸収するためのもの。
"$MY_VIMRUNTIMEはユーザーランタイムディレクトリを示す。
":echo $MY_VIMRUNTIMEで実際のパスを確認できます。
"https://sites.google.com/site/fudist/Home/vim-nihongo-ban/-vimrc-sample
if isdirectory($HOME . '/.vim')
	let $MY_VIMRUNTIME = $HOME.'/.vim'
elseif isdirectory($HOME . '\_vim')
	let $MY_VIMRUNTIME = $HOME.'\_vim'
elseif isdirectory($HOME . '\vimfiles')
	let $MY_VIMRUNTIME = $HOME.'\vimfiles'
endif

filetype off

""" neobundle設定
if has('vim_starting')
	" TODO expandの中身を$MY_VIMRUNTIMEに直す
	if s:is_win
		set runtimepath+=~/vimfiles/bundle/neobundle.vim/
		call neobundle#begin(expand('~/vimfiles/bundle'))
	else 
		set runtimepath+=~/.vim/bundle/neobundle.vim/
		call neobundle#begin(expand('~/.vim/bundle'))
	end
end

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" NeoBundleプラグインのリポジトリ
" vim本体
NeoBundle 'Shougo/unite.vim.git'
NeoBundle 'Shougo/unite-ssh.git'
NeoBundle 'Shougo/neocomplcache.git'
NeoBundle 'Shougo/vimproc.git', {
      \ 'build' : {
      \     'windows' : 'tools\\update-dll-mingw',
      \     'cygwin' : 'make -f make_cygwin.mak',
      \     'mac' : 'make -f make_mac.mak',
      \     'unix' : 'make -f make_unix.mak',
      \    },
      \ }
NeoBundle 'Shougo/vimfiler.vim.git'
NeoBundle 'Shougo/neomru.vim'
NeoBundle 'fuenor/qfixhowm.git'
NeoBundle 'thinca/vim-quickrun.git'
NeoBundle 'vim-scripts/taglist.vim'
NeoBundle 'itchyny/lightline.vim' 
" 編集機能強化
NeoBundle 'kana/vim-fakeclip.git'
" undoをグラフィカル表示
NeoBundle 'sjl/gundo.vim.git'
" yankをリングバッファにする
NeoBundle 'vim-scripts/YankRing.vim'
" プロセス間のyank機能共有
NeoBundle 'vim-scripts/yanktmp.vim'
" スクラッチバッファ
NeoBundle 'vim-scripts/scratch.vim'
" マーク位置表示
"NeoBundle 'vim-scripts/ShowMarks.vim'
" IME制御
NeoBundle 'fuenor/im_control.vim.git'
"NeoBundle 'tyru/operator-camelize.vim.git'

" プログラミング一般
" サラウンド
NeoBundle 'kana/vim-surround.git'
" アウトライン表示
NeoBundle 'vim-scripts/DotOutlineTree.git'
" SourceExplorer
NeoBundle 'wesleyche/SrcExpl.git'
" NERDTree
"NeoBundle 'scrooloose/nerdtree.git'
" コメントアウトショートカット
NeoBundle 'sakuraiyuta/commentout.vim.git'
" カッコの対応
NeoBundle 'vim-scripts/matchit.zip.git'
"置換拡張
NeoBundle 'tpope/vim-abolish.git'
NeoBundle 'tpope/vim-repeat.git'

" VCS
NeoBundle 'tpope/vim-fugitive.git'
NeoBundle 'hgrev'
" HTML
NeoBundle 'mattn/emmet-vim'
"NeoBundle 'taichouchou2/vim-javascript'
NeoBundle 'othree/html5.vim.git'
NeoBundle 'hail2u/vim-css3-syntax'

" twitter
NeoBundle 'basyura/TweetVim'
NeoBundle 'basyura/twibill.vim'
NeoBundle 'tyru/open-browser.vim'
NeoBundle 'h1mesuke/unite-outline'
NeoBundle 'basyura/bitly.vim'
NeoBundle 'mattn/favstar-vim'
NeoBundle 'yomi322/unite-tweetvim'

" syntax
NeoBundle 'cespare/vim-toml'


" 256色表示
set t_Co=256

"colorscheme mycol

call neobundle#end()

filetype plugin indent on

NeoBundleCheck

"---------------------------------------------------------------------------
" 検索の挙動に関する設定
"---------------------------------------------------------------------------
" 検索時に大文字小文字を無視 (noignorecase:無視しない)
set ignorecase
" 大文字小文字の両方が含まれている場合は大文字小文字を区別
set smartcase
" 検索時にファイルの最後まで行ったら最初に戻る (nowrapscan:戻らない)
set wrapscan
" インクリメンタルサーチを使う
set incsearch
"検索語のハイライト
set hlsearch


"---------------------------------------------------------------------------
" 編集に関する設定
"---------------------------------------------------------------------------
" タブをスペースに展開しない (expandtab:展開する)
set noexpandtab
" 自動的にインデントする
set smartindent
" 自動的にタブを入力
set smarttab
" バックスペースでインデントや改行を削除できるようにする
set backspace=2
" コマンドライン補完するときに強化されたものを使う(参照 :help wildmenu)
set wildmenu
" 補完の候補が2つ以上の時は一覧を表示し候補が1つになるまで補完しない
set wildmode=list:longest
" バックアップファイルを作成しない
set nobackup
" 自動リロード
set autoread
" バッファ放棄時に開放
set nohidden
" 矩形選択時は仮想編集可
set virtualedit=block
" クリップボードを共有
set clipboard=unnamed
" 補完時プレビュー表示をしない
set completeopt-=preview
" 改行コード
if s:is_win
	set fileformats=dos,unix,mac
elseif s:is_unix || s:is_mac_x
	set fileformats=unix,dos,mac
end
" UNDO永続化(7.3以降)
if has('persistent_undo')
	let s:undodir = $HOME.'/.vimundo'
	if !isdirectory(s:undodir)
		call mkdir(s:undodir)
	endif
	" UNDO永続化対象のファイル
	set undofile
	" UNDOファイルの保存場所
	set undodir=$HOME/.vimundo
	" 永続化UNDOの可能回数
	set undolevels=1000
	" リロード時にバッファを保存するかどうか
	set undoreload=10000
endif
" *.swpは指定場所に作成
let s:swpdir = $HOME.'/.vimswp'
if !isdirectory(s:swpdir)
	call mkdir(s:swpdir)
endif
set directory=$HOME/.vimswp

" ファイルオープン時カーソル位置復帰
augroup vimrcEx
  au BufRead * if line("'\"") > 0 && line("'\"") <= line("$") |
  \ exe "normal g`\"" | endif
augroup END

" オムニ補完設定
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType ruby set omnifunc=rubycomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType c set omnifunc=ccomplete#Complete
autocmd FileType scala set omnifunc=scalacomplete#CompleteTags

" テンプレートの挿入
autocmd BufNewFile *.py 0r $MY_VIMRUNTIME/template/skel.py

" 開いているファイルのディレクトリをカレントディレクトリにする
autocmd BufEnter * if expand("%:p:h") !~ '^/tmp' | silent! lcd %:p:h | endif

"---------------------------------------------------------------------------
" 表示に関する設定
"---------------------------------------------------------------------------
"タブの表示をデフォルト4文字にする
set tabstop=4 shiftwidth=4 softtabstop=4
"コマンドラインの高さを2に設定
set cmdheight=2
" 括弧入力時に対応する括弧を表示 (noshowmatch:表示しない)
set showmatch
" テキスト挿入中の自動折り返しを日本語に対応させる
set formatoptions+=mM
" 行番号を表示 (number:表示)
set number
" ルーラーを表示 (noruler:非表示)
set ruler
" タブや改行を表示 (list:表示)
set nolist
" 文字の可視化フォーマット指定
set listchars=tab:>-,trail:_,nbsp:%,extends:>,precedes:<,eol:$
set list
" 常にステータス行を表示 (詳細は:he laststatus)
set laststatus=2
" コマンドラインの高さ (Windows用gvim使用時はgvimrcを編集すること)
set cmdheight=2
" コマンドをステータス行に表示
set showcmd
" 長い行を折り返して表示 (nowrap:折り返さない)
set wrap
" タイトルを表示
set title
" diffオプション
" vertical  常時縦分割で表示
" set vertiacal
" filler    存在しない行は"---------"で表示する	
set diffopt=filler
"タブ（機能）を常に表示
set showtabline=2
"ステータスラインについての設定
"設定値のフォーマットの詳細は:help statuslineを参照
set statusline=
" ファイル名(相対)
set statusline+=%-0.40f
" グループ開始
"set statusline+=%(
	" 読み込み専用フラグ
	set statusline+=%r
	" 修正フラグ
	set statusline+=%m
	" ファイルタイプ
	set statusline+=%y
	" ファイルフォーマット
	set statusline+=[%{&fileformat}]
	" エンコーディング
	set statusline+=[%{&fileencoding}]
	" gitの状態
	set statusline+=%{fugitive#statusline()}
" グループ終了
"set statusline+=%)
" 右寄せ・左寄せ区切り
set statusline+=%=
" カーソルの位置情報
" 列の位置(%Vはマルチバイト文字の時に表示)
set statusline+=C:%c%V\ 
" 行の位置（数値）
set statusline+=L:%l/%L\ 
" 行の位置（全体に対するパーセンテージ）
set statusline+=%p%%\ 
"ステータスラインを常に表示
set laststatus=2

" シンタックスカラー
if !exists("syntax_on")
	syntax on
endif
" 2バイト文字があってもカーソル位置がずれないようにする
if exists('&ambiwidth')
  set ambiwidth=double
endif
" カレント行・列のハイライトをしない
set nocursorline
set nocursorcolumn
" 全角スペースをハイライトさせる。
" refer:http://d.hatena.ne.jp/potappo2/20061107/1162862536
function! JISX0208SpaceHilight()
    syntax match JISX0208Space "　" display containedin=ALL
    highlight JISX0208Space term=underline ctermbg=DarkGray guibg=DarkGray
endf
"syntaxの有無をチェックし、新規バッファと新規読み込み時にハイライトさせる
if has("syntax")
    syntax on
        augroup invisible
        autocmd! invisible
        autocmd BufNew,BufRead * call JISX0208SpaceHilight()
    augroup END
endif

" encoding.vimの日本語を含まない場合にcp932にする設定をオフ
let loaded_ReCheckFENC = 1

"---------------------------------------------------------------------------
" キー設定
"---------------------------------------------------------------------------
" グローバルマップキーを","に変更する
let mapleader = ","
" QuickFixウインドウ設定
autocmd QuickfixCmdPost * copen
"*でハイライトしても次の単語に移らない
"一瞬移動するのがわかるけど仕方ない。
nmap * *N
" タブまわりショートカットキー
if version >= 700
	nnoremap <silent> <C-Tab>   gt
	nnoremap <silent> <C-S-Tab> gT
	nmap <silent> tn gt
	nmap <silent> tp gT
	nmap tm <Esc>:tabmove
	nmap te <Esc>:tabedit
endif


"---------------------------------------------------------------------------
" その他設定
"---------------------------------------------------------------------------

"現バッファの差分表示。
command! DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis | wincmd p | diffthis
"ファイルまたはバッファ番号を指定して差分表示。#なら裏バッファと比較
command! -nargs=? -complete=file Diff if '<args>'=='' | browse vertical diffsplit|else| vertical diffsplit <args>|endif


"---------------------------------------------------------------------------
" プラグイン設定
"---------------------------------------------------------------------------

"-----------------------------------------------
" QFixHowm
set shellslash
let QFixHowm_Key         = ','
let QFix_Autoformat	 = 0
let howm_filename        = '%Y/%m/%Y-%m-%d-%H%M%S.howm'
let howm_dir             = $HOME.'/.howm/'
if !isdirectory(howm_dir)
	call mkdir(howm_dir)
endif
if s:is_win
  let howm_fileencoding    = 'cp932'
  let howm_fileformat      = 'dos'
elseif s:is_unix || s:is_mac_x
  let howm_fileencoding    = 'utf-8'
  let howm_fileformat      = 'unix'
endif
nnoremap <silent> ,,hh :echo g:howm_dir<CR>
nnoremap <silent> ,,ha :call HowmChEnv('',     'time', '=')<CR>
nnoremap <silent> ,,hm :call HowmChEnv('main', 'time', '=')<CR>
nnoremap <silent> ,,hw :call HowmChEnv('work', 'time', '=[work]')<CR>

"-----------------------------------------------
" QFixGrep
" 検索除外対象
let MyGrep_ExcludeReg = '[~#]$\|\.bak$\|\.o$\|\.obj$\|\.exe$\|[/\\]tags$\|[/\\]\.svn[/\\]|[/\\]\.hg[/\\]|[/\\]\.settings[/\\]'
let mygrepprg = 'grep'
if s:is_win
  let MyGrep_ShellEncoding = 'cp932'
elseif s:is_unix || s:is_mac_x
  let MyGrep_ShellEncoding = 'utf8'
endif

"-----------------------------------------------
" neocomplcache
" スタート時に読み込む
let g:neocomplcache_enable_at_startup = 1 
" 数字で候補選択しない
" 日本語入力で数値を入れるときに問題になるのでオフ
let g:neocomplcache_enable_quick_match = 0
" スニペット展開ショートカットキー
imap <C-k> <Plug>(neocomplcache_snippets_expand)
smap <C-k> <Plug>(neocomplcache_snippets_expand)

"-----------------------------------------------
" netrw
" netrwでブラウズしたディレクトリをカレントディレクトリにする
let g:netrc_keepdir = 0

"-----------------------------------------------
" yankring
" yankringのファイル名
let g:yankring_history_file = '.yankring_history'


"-----------------------------------------------
" taglist
" ctagsのパス
if s:is_win
  set tags=tags
endif
" actionscript language
let tlist_actionscript_settings = 'actionscript;c:class;f:method;p:property;v:variable'
let g:SrcExpl_RefreshTime = 0
" // Set the height of Source Explorer window
let g:SrcExpl_winHeight = 8
" // Set 100 ms for refreshing the Source Explorer
let g:SrcExpl_refreshTime = 100
" // Set "Enter" key to jump into the exact definition context
let g:SrcExpl_jumpKey = "<ENTER>"
" // Set "Space" key for back from the definition context
let g:SrcExpl_gobackKey = "<SPACE>"
" // In order to Avoid conflicts, the Source Explorer should know what plugins
" // are using buffers. And you need add their bufname into the list below
" // according to the command ":buffers!"
let g:SrcExpl_pluginList = [ 
        \ "__Tag_List__", 
        \ "Source_Explorer" 
    \ ] 
" // Enable/Disable the local definition searching, and note that this is not
" // guaranteed to work, the Source Explorer doesn't check the syntax for now.
" // It only searches for a match with the keyword according to command 'gd'
let g:SrcExpl_searchLocalDef = 1
" // Let the Source Explorer update the tags file when opening
let g:SrcExpl_isUpdateTags = 0
" // Use program 'ctags' with argument '--sort=foldcase -R' to create or
" // update a tags file
let g:SrcExpl_updateTagsCmd =  'ctags --sort=foldcase -R .'
" // Set "<F12>" key for updating the tags file artificially
let g:SrcExpl_updateTagsKey = "<F5>"
" // The switch of the Source Explorer 
nmap <silent> <F12> :SrcExplToggle<CR> 
" Open and close the taglist.vim separately 
nmap <silent> <F11> :TlistToggle<CR> 
" Open and close the nerdtree.vim separately 
"nmap <silent> <F10> :NERDTreeToggle<CR> 


"-----------------------------------------------
" scratch utility
" ショートカットキー割り当て
nmap <silent> <F8> <Plug>ShowScratchBuffer
imap <silent> <F8> <Plug>InsShowScratchBuffer


"-----------------------------------------------
" netrw
" カーソルラインを表示しない
let g:netrw_cursorline = 0
" 初期表示は詳細表示
let g:netrw_liststyle = 1
" ブラウジングでスワップファイルを作らない
let g:netrw_use_noswf = 1

"-----------------------------------------------
" DotOutlineTree
" ショートカットキー
nmap  <silent>  <F9>  :DotOutlineTree<CR>

"-----------------------------------------------
" quickrun.vim
let g:quickrun_config = {
\	"_":{
\		"runner": "vimproc",
\		"runner/vimproc/updatetime": 60
\	},
\	'*':{
\		"outputter/buffer/into": 1,
\		"outputter/buffer/split": "below",
\	},
\}

" windows用
if s:is_win
	" shebang無視
	let g:quickrun_config['*']["hook/shebang/enable"] = 0
	" エンコードをSJISに変更
	let g:quickrun_config['*']["hook/output_encode/enable"] = 1
	let g:quickrun_config['*']["hook/output_encode/encoding"] = "cp932"
endif

"-----------------------------------------------
" unite.vim
" 起動時にインサートモードで開始
let g:unite_enable_start_insert = 1
" 候補絞込みの更新間隔
let g:unite_update_time = 100

" インサート／ノーマルどちらからでも呼び出せるようにキーマップ
" いつもの
nnoremap <silent> ,uu :<C-u>UniteWithBufferDir -buffer-name=files file file/new file_mru<CR>
" バッファ一覧
nnoremap <silent> ,ub :<C-u>Unite buffer<CR>
" ブックマーク一覧
nnoremap <silent> ,um :<C-u>Unite bookmark<CR>
" ファイル一覧
nnoremap <silent> ,uf :<C-u>Unite -buffer-name=files file file/new<CR>
" 最近使用したファイル一覧
nnoremap <silent> ,ur :<C-u>Unite file_mru<CR>
" 全部乗せ
nnoremap <silent> ,ua :<C-u>UniteWithBufferDir -buffer-name=files buffer file_mru bookmark file<CR>

" unite.vim上でのキーマッピング
autocmd FileType unite call s:unite_my_settings()
function! s:unite_my_settings()
  " 単語単位からパス単位で削除するように変更
  imap <buffer> <C-w> <Plug>(unite_delete_backward_path)
  " ESCキーを2回押すと終了する
  nmap <silent><buffer> <ESC><ESC> q
  imap <silent><buffer> <ESC><ESC> <ESC>q
  " 絶対パス表示切替
  inoremap <silent><buffer><expr> <C-@> unite#do_action('absolute_path')
endfunction

call unite#custom#profile('files', 'substitute_patterns', {
      \ 'pattern' : '^\~',
      \ 'subst' : substitute(
      \     unite#util#substitute_path_separator($HOME),
      \           ' ', '\\\\ ', 'g'),
      \ 'priority' : -100,
      \ })
call unite#custom#profile('files', 'substitute_patterns', {
      \ 'pattern' : '\.\{2,}\ze[^/]',
      \ 'subst' : "\\=repeat('../', len(submatch(0))-1)",
      \ 'priority' : 10000,
      \ })

" refer:http://blog.basyura.org/entry/2013/05/08/210536
let my_absolute_path = {
\ 'is_selectable' : 1,
\ 'is_quit' : 0,
\ }
function! my_absolute_path.func(candidates)
  let candidate = a:candidates[0]
  let path      = candidate.action__path
  if candidate.kind == 'directory'
    let path = fnamemodify(path . '/../', ':p')
  else
    let path = fnamemodify(path, ':p:h') . '/'
  end
  call unite#start([['file'], ['file/new']],unite#get_context())
  call unite#mappings#narrowing(path)
endfunction
call unite#custom_action('file', 'absolute_path', my_absolute_path)
unlet my_absolute_path

"-----------------------------------------------
" showmarks.vim
" マークの表示パターン
let g:showmarks_enable = 1
let g:showmarks_include = 'abcdefghijklnmopqrstuvxyzABCDEFGHIJKLNMOPQRSTUVXYZ'


"-----------------------------------------------
" gundo.vim
" ショートカットキー
nmap  <silent>  <F7>  :GundoToggle<CR>


"-----------------------------------------------
" lightline.vim
" 外観
let g:lightline = {
	\ 'colorscheme': 'wombat',
	\}
