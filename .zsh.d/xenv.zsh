# プログラミング言語の実行系の分離するプラグインの設定
# direnvは目的が違うのでここに設定を書かないこと

## anyenv
#if type anyenv &> /dev/null; then
#    export PATH="$HOME/.anyenv/bin:$PATH"
#    eval "$(anyenv init -)"
#fi

## jenv
#JENV_PATH=$HOME/.jenv
#if [ -f $JENV_PATH ]; then
#   export PATH="$JENV_PASTH:$PATH"
#   eval "$(jenv -init -)"
#fi

## nodenv
#NODENV_PATH=$HOME/.nodenv
#if [ -d $NODENV_PATH ]; then
#    export PATH=$NODENV_PATH/bin:$NODENV_PATH/shims:$PATH
#    eval "$(nodenv init -)"
#fi
#
## ruby
#RBENV_PATH=$HOME/.rbenv
#if [ -f $RBENV_PATH ]; then
#    export PATH=$RBENV_PATH/bin:$PATH
#    eval "$(rbenv init -)"
#fi
