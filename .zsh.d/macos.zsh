# casksのpinが使えず、upgradeでcaskもアップグレードされるようになったので回避策
alias brew-upgrade="sort <(brew outdated | awk '{print}') <(cat $HOME/.hombrew_upgrade_exclude_packages) | uniq -u | xargs brew upgrade"

# 明示的にインストールしたパッケージの表示
alias brew-list-installed-req="brew info --installed --json=v1 | jq -r '.[] | select(.installed | any(.installed_on_request)) | .name'"

function dump-defaults() {
	echo $(defaults domains) | tr -s ',' '\n' | while read domain; do; defaults read "$domain"; done;
}

# force homebrew cask isntallation path
#export HOMEBREW_CASK_OPTS="--appdir=/Applications"

HOMEBREW=/opt/homebrew

# add path to use homebrew installed apps
export PATH=$HOMEBREW/bin:$HOMEBREW/sbin:$PATH

# load homebrew installed completions
export FPATH=$FPATH:$(brew --prefix)/share/zsh/site-functions


