# for mac ls colorize
export CLICOLOR=1

# directory moving
#setopt auto_pushd
#setopt auto_cd

ENHANCD_FILTER=fzy:fzf:peco
export ENHANCD_FILTER

export PATH="/usr/local/sbin:$PATH"

# stdout visible EOL
# traditional style
#unsetopt prompt_cr prompt_sp
# define EOL char
export PROMPT_EOL_MARK=

# enable zsh-completions
#autoload -U compinit && compinit

# Enable bash-style completion.
autoload -U +X bashcompinit && bashcompinit


# word delete shortcut key (c-w) will stop front of shell slash
WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'
export WORDCHARS

