#tmux
if [ $SHLVL = 1 ]; then
    alias t="tmux attach || tmux -2 new \; source-file ~/.tmux/new-session"
fi
alias tmux='tmux -2'
