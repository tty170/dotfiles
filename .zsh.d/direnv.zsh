if type direnv &> /dev/null; then
    export EDITOR=vim
    eval "$(direnv hook zsh)"
fi

alias da='direnv allow'