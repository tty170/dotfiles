# python
#export WORKON_HOME=$HOME/.virtualenvs
export PIPENV_VENV_IN_PROJECT=true
#VIRTUALENVWRAPPER=/usr/local/bin/virtualenvwrapper.sh
#if [ -f $VIRTUALENVWRAPPER ]; then
#    export PATH=/usr/local/bin:$PATH
##   export VIRTUALENVWRAPPER_PYTHON=`which python3`
#    source $VIRTUALENVWRAPPER
#fi

show_virtual_env() {
  if [ -n "$VIRTUAL_ENV" ]; then
    echo "($(basename $VIRTUAL_ENV))"
  fi
}

PS1='$(show_virtual_env)'$PS1
