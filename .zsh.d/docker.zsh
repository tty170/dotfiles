# docker
if type docker &> /dev/null; then
	alias docker-images-refresh="docker image ls -a --format=\"{{.Repository}}:{{.Tag}}\" | grep -v '<none>' | xargs -L1 docker pull"
fi

docker-image-tag-ls () {
	curl -s https://registry.hub.docker.com/v1/repositories/$1/tags | jq -r '.[].name'
}

docker-images-dump () {
	docker image ls --format="{{.Repository}}:{{.Tag}}"
}

# force arch for desktop mac
#export DOCKER_DEFAULT_PLATFORM=linux/amd64

source $HOME/.docker/init-zsh.sh || true # Added by Docker Desktop
