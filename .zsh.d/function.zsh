# 製品に特化してない関数とかの定義


function backup() {
    if [ $# -eq 0 ]; then
        echo "Usage: backup [file or directory name]"
        return 1
    else
        # TODO pass suffix
        # TODO pass multiple files or dirs
        if [ ! -e $1 ]; then
            echo "file or directory does not exists."
            return 1
        elif [ -d $1 ]; then
            mv "$1" "$1_$(date '+%Y-%m-%d_%H%M%S')_backup"
        elif [  -f $1 ]; then
            mv "$1" "$1_$(date '+%Y-%m-%d_%H%M%S').bak"
        fi
    fi
}
