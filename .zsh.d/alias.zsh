# alias
alias l='ls'
alias ll='ls -l'
alias la='ls -a'
alias mv='mv -i'
alias cp='cp -i'
alias rm='rm -i'

alias history='history -Di -E 1'

alias gip='dig -4 +short myip.opendns.com A @resolver1.opendns.com'
alias gip6='dig -6 +short myip.opendns.com AAAA @resolver1.ipv6-sandbox.opendns.com'
alias lip='ifconfig en0 | grep inet | awk '\''$1=="inet" {print $2}'\'

# TODO resolve conflict .gitconfig alias and hub alias
#alias git=hub
