function __git_root() {
    # git >=.13
    ROOTDIR=`git rev-parse --show-superproject-working-tree --show-toplevel 2> /dev/null | head -1`
    if [ $? -ne 0 ]; then
        # set empty
        ROOTDIR=""
    fi
    SUBROOTDIR=`git rev-parse --show-toplevel 2> /dev/null`
    if [ $? -ne 0 ]; then
        # set empty
        SUBROOTDIR=""
    fi
    export GIT_ROOT=$ROOTDIR
    export GIT_SUBROOT=$SUBROOTDIR
}

autoload -Uz add-zsh-hook
add-zsh-hook chpwd __git_root
