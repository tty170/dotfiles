HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=100000

setopt inc_append_history
setopt extended_history
# 履歴が共有されると別ターミナルで履歴を遡る時に邪魔になるのでオフ
#setopt share_history
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_verify


# reverse history search
function select-history() {
  #BUFFER=$(fc -l 1 | cut -f 3- -d' ' | fzf --no-sort +m --query "$LBUFFER" --prompt="History > ")
  BUFFER=$(fc -ln 1 | fzy --query "$LBUFFER" )
  CURSOR=$#BUFFER
}
zle -N select-history

# histiry search setting
bindkey '^r' select-history
bindkey '^P' history-substring-search-up
bindkey '^N' history-substring-search-down


# 成功したコマンドのみ履歴に残す。
# また除外条件に合致する場合は残さない。
# ref: http://someneat.hatenablog.jp/entry/2017/07/25/073428
__record_command() {
  typeset -g _LASTCMD=${1%%$'\n'}
  return 2	# 0:save HISTFILE, 1:do not save, 2: save internal history
}
zshaddhistory_functions+=(__record_command)

__update_history() {
  local last_status="$?"
  local line=${_LASTCMD}
  local cmd=${line%% *}

  # hist_ignore_space
  if [[ ! -n ${cmd} ]]; then
    return
  fi

  # exclude commands
  if [[
  ${line} == (ls)		# exclude ls ('ls' ONLY)
  || ${cmd} == (man)	# exclude man
  ]]; then
    return
  fi

  # hist_reduce_blanks
  local cmd_reduce_blanks=$(echo ${_LASTCMD} | tr -s ' ')

  # Record the commands that have succeeded
  if [[ ${last_status} == 0 ]]; then
    print -sr -- "${cmd_reduce_blanks}"
  fi
}
precmd_functions+=(__update_history)
