# load hosts when press tab key auto complete
_cache_hosts=(`cat $HOME/.ssh/known_hosts | awk '{print $1}' | sed 's/,.*//g' | sed -E 's/\[(.*)\]:[0-9]+/\1/g' | sort | uniq`)

# avoid prompt PIN input
#resident_keys=$(ssh-add -l | cut -d ' ' -f 3,4 | grep -E '^ \(ED25519-SK\)$')
#echo -n "$resident_keys"
#if [ -z "$resident_keys" ]; then
#	echo "To add resident private key from FIDO authenticator, execute run 'ssh-add -K' manually."
#fi

# launch ssh-gent
SSH_AGENT_FILE=$HOME/.ssh-agent


function ssh-agent-launch() {
	test -f $SSH_AGENT_FILE && source $SSH_AGENT_FILE
	if ! ssh-add -l > /dev/null 2>&1; then
		ssh-agent | grep -v 'echo' > $SSH_AGENT_FILE
		source $SSH_AGENT_FILE
		# default load private keys from 
		ssh-add > /dev/null 2>&1
		#ssh-add $HOME/.ssh/id_rsa > /dev/null 2>&1
		#ssh-add $HOME/.ssh/id_ed25519_sk > /dev/null 2>&1
	fi
}

function ssh-agent-kill() {
	killall -u $USER ssh-agent
	rm $SSH_AGENT_FILE
}

ssh-agent-launch
