
if type aws &> /dev/null; then

function ecr-login() {
    if [ $# -eq 0 ]; then
        eval $(aws ecr get-login --no-include-email --region ap-northeast-1)
    else
        eval $(aws ecr get-login --no-include-email --region ap-northeast-1 --profile $1)
    fi
}


function whoamiaws() {
    if [ $# -ge 2 ]; then
        echo "Usage: whoamiaws [profile]"
        return 1
    else
        if [ $# -eq 1 ]; then
            aws sts get-caller-identity --query "Arn" --output text --profile $1
        else
            aws sts get-caller-identity --query "Arn" --output text
        fi
    fi
}

fi