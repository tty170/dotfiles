.. coding:utf-8

ホスト別の固有設定
==================

hosts/hostsに配置する。


.gitignoreについて
=====================

.vim/bundle配下
---------------

このディレクトリはvimプラグインのneobundleで管理するので除外。


追加する必要があるモジュール
============================

後でサブモジュールにするかも

vim関連
-------

* git://github.com/Shougo/neobundle.vim.git
* git://github.com/Shougo/vimproc.git
