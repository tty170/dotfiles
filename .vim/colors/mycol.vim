hi clear Normal
set bg&

hi clear

if exists('syntax_on')
    syntax reset
endif

let colors_name = "mycol"

hi LineNr ctermfg=3 guifg=darkyellow
hi SpecialKey term=bold cterm=bold ctermfg=darkgrey gui=bold guifg=darkgrey
hi NonText term=bold cterm=bold ctermfg=darkgrey gui=bold guifg=darkgrey
hi Comment ctermfg=21 guifg=blue
hi Statement ctermfg=11 guifg=#ffff60

