" reStructured Text File indent settings.

if exists("b:did_indent")
  finish
endif
let b:did_indent = 1

" DO NOT use hard tab.
set expandtab

set tabstop=3
set softtabstop=0
set shiftwidth=3

