" vimrc file for following the coding standards specified in PEP 7 & 8.
"
"  This file imported from Python-2.6.6/Misc/Vim/vimrc.
"
" Number of spaces to use for an indent.
" This will affect Ctrl-T and 'autoindent'.
set shiftwidth=4

" Number of spaces that a pre-existing tab is equal to.
" For the amount of space used for a new tab use shiftwidth.
set tabstop=8

" Replace tabs with the equivalent number of spaces.
" Also have an autocmd for Makefiles since they require hard tabs.
set expandtab

" Use the below highlight group when displaying bad whitespace is desired
highlight BadWhitespace ctermbg=red guibg=red

" Display tabs at the beginning of a line in Python mode as bad.
match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
match BadWhitespace /\s\+$/

" Use UNIX (\n) line endings.
set fileformat=unix
" File encoding is UTF-8.
set fileencoding=utf8
