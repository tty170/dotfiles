"actionscript and mxml filetype
"vim:ts=8 sw=8 sts=4
au BufNewFile,BufRead	*.as	set filetype=actionscript
au BufNewFile,BufRead	*.mxml	set filetype=mxml
