"---------------------------------------------------------------------------
" フォント設定:
"---------------------------------------------------------------------------
if has('win32')
  " Windows用
  " VL GothicのDLは以下へ
  " http://vlgothic.dicey.org/
  set guifont=VL_Gothic:h10:cSHIFTJIS
  "set guifont=MS_Gothic:h11:cSHIFTJIS
  " 行間隔の設定
  set linespace=1
elseif has('mac') || has('gui_macvim')
  set guifont=Osaka-Mono:h14
  set guifontwide=Osaka-Mono:h14
elseif has('xfontset')
  " UNIX用 (xfontsetを使用)
  set guifontset=a14,r14,k14
endif

"---------------------------------------------------------------------------
" GUI固有の表示設定
"---------------------------------------------------------------------------
" 背景を黒、文字を白にする
colorscheme evening
hi SpecialKey guifg=DarkGray guibg=NONE
hi NonText gui=bold guifg=DarkGray guibg=NONE
"タブ表示（機能）についての設定
"設定値のフォーマットの詳細は:help statuslineを参照
set guitablabel=%N\ %(%m\ %)%f
" 入力中はマウスカーソル非表示
set mousehide
" ウインドウの幅
set columns=110
" ウインドウの高さ
set lines=30
" beep off for (g)vim
set visualbell t_vb=

"---------------------------------------------------------------------------
" 日本語入力に関する設定:
"---------------------------------------------------------------------------
" kaoriyaから輸入
if has('multi_byte_ime') || has('xim') || has('mac') || has('gui_macvim')
	" IME ON時のカーソルの色を設定(設定例:紫)
	highlight CursorIM guibg=Purple guifg=NONE
	" 挿入モード・検索モードでのデフォルトのIME状態設定
	set noimdisable
	set iminsert=0 imsearch=0
	if has('xim') && has('GUI_GTK')
		" XIMの入力開始キーを設定:
		" 下記の s-space はShift+Spaceの意味でkinput2+canna用設定
		"set imactivatekey=s-space
	endif
	" 挿入モードでのIME状態を記憶させない場合、次行のコメントを解除
	"inoremap <silent> <ESC> <ESC>:set iminsert=0<CR>
endif
